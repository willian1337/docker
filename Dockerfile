FROM php:7.0-apache
LABEL Name=will/dev Version=1.0.0

# Instalando o basico
RUN apt-get update && apt-get install -y vim git locales wget git 

# Instala o unoconv para converter documentos em pdf
RUN apt-get install -y libreoffice-core libreoffice-common libreoffice-writer unoconv
COPY templates/unoconvd /etc/init.d/
RUN chmod 755 /etc/init.d/unoconvd
RUN update-rc.d  unoconvd defaults
RUN service unoconvd start

# Instala gd extensão utilizado para redimensionar as imagens de prefil
RUN apt-get install -y libpng-dev libfreetype6-dev libjpeg-dev
RUN docker-php-ext-configure gd \
    --with-freetype-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ 
RUN docker-php-ext-install gd

# Instala o cURL
RUN apt-get install -y libcurl4-openssl-dev
RUN docker-php-ext-install curl

# Instala o restante das extensões
RUN apt-get install -y zlib1g-dev zip 
RUN docker-php-ext-install mysqli pdo_mysql iconv zip exif mbstring

# Extensão para o funcionamento do websocket
RUN apt-get install -y libzmq3-dev 
RUN pecl install zmq-beta
RUN docker-php-ext-enable zmq

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Configura o apache
RUN a2enmod rewrite
RUN chown -R www-data:www-data /var/www
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

# Envia o php.ini
COPY templates/php.ini /usr/local/etc/php/

# Criando a maquina virtual
COPY templates/virtualhosts.conf /etc/apache2/sites-available/
RUN a2ensite virtualhosts.conf 
COPY templates/virtualhosts_api.conf /etc/apache2/sites-available/
RUN a2ensite virtualhosts_api.conf 
RUN a2enmod proxy 
RUN a2enmod proxy_http 

# Seta a localização pt_BR
RUN echo "pt_BR.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen
ENV LANG pt_BR.UTF-8
ENV LC_ALL pt_BR.UTF-8
